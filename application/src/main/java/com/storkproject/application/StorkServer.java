package com.storkproject.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StorkServer {
  public static void main(String[] args) {
    SpringApplication.run(StorkServer.class, args);
  }
}
