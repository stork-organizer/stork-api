package com.storkproject.application.rest.event;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.ZonedDateTime;
import java.util.Objects;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity(name = "stork_event")
@Getter
@Setter
@ToString
public class Event {
  @Id
  @Column(name = "event_id", nullable = false, length = 36)
  private String eventId;

  @Column(name = "event_type", nullable = false, length = 36)
  private String eventType;

  @Column(name = "title", nullable = false)
  private String title;

  @Column(name = "notes", nullable = false)
  private String notes;

  @Column(name = "start_date", nullable = false)
  private ZonedDateTime startDate;

  @Column(name = "end_date", nullable = false)
  private ZonedDateTime endDate;

  @Column(name = "organizer", nullable = false, length = 36)
  private String organizer;

  @Column(name = "team" , length = 36)
  private String team;

  @Column(name = "recurrence")
  private String recurrence;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    Event event = (Event) o;
    return eventId != null && Objects.equals(eventId, event.eventId);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
