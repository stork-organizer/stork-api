package com.storkproject.application.rest.event;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, String> {
  List<Event> findByOrganizer(String organizer);

  Event findByEventId(String eventId);
}
