package com.storkproject.application.rest.user;

import com.storkproject.application.rest.user.model.UserData;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
  List<UserData> data = new ArrayList<>(List.of(
      new UserData("root", "root@localhost", "root")
          .uuid("c2572532-bc09-42e9-b8c0-efb78ef96d7f")
          .roles("OWNER", "ADMIN", "USER"),
      new UserData("admin", "admin@localhost", "admin")
          .uuid("6feec3a9-6444-40b6-be6c-cdc92d52dd56")
          .roles("ADMIN", "USER"),
      new UserData("user", "user@localhost", "user")
          .uuid("94480146-af88-4331-bc04-53a762eabaf3")
          .roles("USER")
  ));

  public void renameUser(String id, String name){
    Optional<UserData> u = data.stream().filter(user ->user.getUuid().equals(id)).findFirst();
    u.ifPresent(userData -> userData.setUsername(name));
  }

  public void changeEmail(String id, String email){
    Object principal = SecurityContextHolder.getContext()
            .getAuthentication().getPrincipal();
    if(principal instanceof UserDetails userDetails) {
      Optional<UserData> u = data.stream().filter(user -> user.getUuid().equals(id)).findFirst();
      u.ifPresent(userData -> userData.setMail(email.split("\"")[3]));
    }
  }

  public List<UserData> getUsers() {
    return data;
  }

  public Collection<UserDetails> getUserDetails() {
    return new ArrayList<>(this.data);
  }

  public String currentlyRegisteredUser() {
    Object principal = SecurityContextHolder.getContext()
        .getAuthentication().getPrincipal();
    if(principal instanceof UserDetails userDetails) {
      Optional<UserData> data = this.data.stream()
          .filter(user -> user.getUsername().equals(userDetails.getUsername()))
          .findFirst();
      if(data.isPresent()) {
        return data.get().getUuid();
      } else {
        return userDetails.getUsername();
      }
    }
    return null;
  }

  public String currentlyRegisteredUserData() {
    Object principal = SecurityContextHolder.getContext()
            .getAuthentication().getPrincipal();
    if(principal instanceof UserDetails userDetails) {
      Optional<UserData> data = this.data.stream()
              .filter(user -> user.getUsername().equals(userDetails.getUsername()))
              .findFirst();
      if(data.isPresent()) {
        return (data.get().getUsername() + " " + data.get().getMail());
      } else {
        return null;
      }
    }
    return null;
  }
}
