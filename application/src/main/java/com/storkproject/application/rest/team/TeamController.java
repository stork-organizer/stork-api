package com.storkproject.application.rest.team;


import com.storkproject.application.rest.team.model.TeamData;
import com.storkproject.application.rest.user.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TeamController {

    TeamService teamService;

    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/teams")
    public List<TeamData> getAllTeams() {
        return teamService.getAllTeams();
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/teams/edit")
    public void editTeam(@RequestBody TeamData td) {
        teamService.editTeam(td);
    }
}
