package com.storkproject.application.rest.event;

import com.storkproject.application.rest.event.dto.EventCreateDto;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EventController {
  private final EventService service;

  public EventController(EventService service) {
    this.service = service;
  }

  @CrossOrigin(origins = "*")
  @GetMapping("/event/{id}")
  public List<Event> getEvents(@PathVariable String id) {
    return service.getEventByOrganizer(id);
  }

  @CrossOrigin(origins = "*")
  @PostMapping("/event")
  public Event updateEvent(@RequestBody EventCreateDto body) {
    return service.createEvent(body);
  }

  @CrossOrigin(origins = "*")
  @PostMapping("/event/delete/{id}")
  public boolean deleteEvent(@PathVariable String id) {
    return service.deleteEvent(id);
  }

  @CrossOrigin(origins = "*")
  @PostMapping("/event/update/{id}")
  public Event modifyEvent(@RequestBody EventCreateDto body,@PathVariable String id) {
    return service.modifyEvent(id, body);
  }
}
