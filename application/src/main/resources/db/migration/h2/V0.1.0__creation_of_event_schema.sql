CREATE TABLE IF NOT EXISTS `stork_event` (
  `event_id` VARCHAR(36) PRIMARY KEY NOT NULL,
  `event_type` VARCHAR(36) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `notes` VARCHAR(255) NOT NULL,
  `start_date` TIMESTAMP WITH TIME ZONE NOT NULL,
  `end_date` TIMESTAMP WITH TIME ZONE NOT NULL,
  `organizer` VARCHAR(36) NOT NULL,
  `team` VARCHAR(36),
  `recurrence` VARCHAR(255)
);