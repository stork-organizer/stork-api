package com.storkproject.application.rest.event;

import com.storkproject.application.rest.event.dto.EventCreateDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.ZonedDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class EventServiceTest {
  @Autowired
  private EventService eventService;

  @Test
  void getUserById() {
    EventCreateDto data = new EventCreateDto(
        "Meeting", "Spring Planning",
        "The meeting is about Spring Planning",
        ZonedDateTime.parse("2020-01-01T00:00:00Z"),
        ZonedDateTime.parse("2020-01-01T00:00:00Z"),
        "ca54542f-e2f4-4993-bb92-a8105081eb89"
    );
    Event event = eventService.createEvent(data);
    assertNotNull(event);
    assertEquals(data.getEventType(), event.getEventType());
    assertEquals(data.getTitle(), event.getTitle());
    assertEquals(data.getNotes(), event.getNotes());
    assertEquals(data.getStartDate(), event.getStartDate());
    assertEquals(data.getEndDate(), event.getEndDate());
    assertEquals(data.getOrganizer(), event.getOrganizer());
    assertNull(event.getTeam());
    assertNull(event.getRecurrence());

    List<Event> retrievedEvent = eventService
        .getEventByOrganizer("ca54542f-e2f4-4993-bb92-a8105081eb89");
    assertFalse(retrievedEvent.isEmpty());
    assertNotNull(retrievedEvent.get(0));
    assertEquals(event, retrievedEvent.get(0));
  }
}