variable "environment" {
  type        = string
  description = "Deployment Environment"

  default = "develop"

  validation {
    condition     = contains(["develop", "prod"], var.environment)
    error_message = "The environment must be either production (prod) or development (develop)."
  }
}

variable "os" {
  type        = string
  description = "Operating System"
  default     = "win"

  validation {
    condition     = contains(["win", "unix"], var.os)
    error_message = "The Operating System must be either Windows (win) or Linux/Mac Os (unix)."
  }
}

variable "project" {
  type        = string
  description = "Project Name"
  default     = "stork"
}
