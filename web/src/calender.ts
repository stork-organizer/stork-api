import {writable} from "svelte/store";

export default class calenderObj {
    today: Date = new Date();
    days: Date[] = [];
    monthForTool = this.today.getMonth();
    yearForTool = this.today.getFullYear();

    constructor() {
        for(let i = 1; i < 8; i++){
            this.days[i] = new Date(this.today);
        }

        let k: number = 0;
        for(let i = this.today.getDay(); i <= 7; i++){
            this.days[i].setDate(this.today.getDate() + k++);
        }

        let l: number = 0;
        for(let i = this.today.getDay(); i >= 1; i--){
            this.days[i].setDate(this.today.getDate() - l++);
        }
    }

    recalculateWeek(left:boolean){
        if(left){
            this.today.setDate(this.today.getDate() - 6)
        }
        else {
            this.today.setDate(this.today.getDate() + 7)
        }
        let k: number = 0;
        for(let i = this.today.getDay(); i <= 7; i++){
            this.days[i].setDate(this.today.getDate() + k++);
        }

        let l: number = 0;
        for(let i = this.today.getDay(); i >= 1; i--){
            this.days[i].setDate(this.today.getDate() - l++);
        }
        this.monthForTool = this.today.getMonth();
        this.yearForTool = this.today.getFullYear();
        console.log(this.days)
    }

    recalculateWeekLeft(){
        this.recalculateWeek(true)
    }

    recalculateWeekRight(){
        this.recalculateWeek(false)
    }
}