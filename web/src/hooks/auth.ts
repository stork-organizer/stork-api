import axios from "axios";
import {UserData, user, EventObj, UserEvents, CreateEventObj, team, AllTeams} from "../stores";

let current_user: UserData;
user.subscribe(value => {
    current_user = value;
})

let current_user_events: EventObj[];
UserEvents.subscribe(value => {
    current_user_events = value;
})

let current_AllTeams: team[];
AllTeams.subscribe(value => {current_AllTeams = value;})

let baseurl: string = "http://localhost:9000";


export const getUserDetails = async () => {
    try {
        const resp:string = (await axios.get(baseurl + "/userdata")).data;
        let newR:string[] = resp.split(' ');
        current_user.setEmail(newR[1]);
        current_user.setUsername(newR[0]);
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
}

export const getUserEvents = async () => {
    try {
        const resp:EventObj[] = (await axios.get(baseurl + "/event/" + current_user.id)).data;
        UserEvents.update(value => {
            return resp;
        })
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
}

export const changeEmail = async (mail: string) => {
    try {
        const resp:EventObj[] = (await axios.post(baseurl + "/user/" + current_user.id + "/mail", {mail})).data;
        current_user.email = mail;
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
}

export const addNewEvent = async (create: CreateEventObj) => {
    let resp;
    try {
         resp = await axios.post(baseurl + "/event",{
            "eventType": create.eventType,
            "title": create.title,
            "notes": create.notes,
            "startDate": create.startDate,
            "endDate": create.endDate,
            "team": create.team,
            "recurrence": create.recurrence
        });
        await getUserEvents();
        return 200;
    } catch (err) {
        // Handle Error Here
        console.error(err);
        return err.response.status;
    }
}

export const modifyEvent = async (id:string, update: CreateEventObj) => {
    let resp;
    try {
        resp = await axios.post(baseurl + "/event/update/" + id,{
            "eventType": update.eventType,
            "title": update.title,
            "notes": update.notes,
            "startDate": update.startDate,
            "endDate": update.endDate,
            "team": update.team,
            "recurrence": update.recurrence
        });
        await getUserEvents();
        return 200;
    } catch (err) {
        // Handle Error Here
        console.error(err);
        return err.response.status;
    }
}

export const deleteOldEvent = async (id: string) => {
    try {
        let resp = await axios.post(baseurl + "/event/delete/" + id);
        await getUserEvents();
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
}

export const getTeams = async () => {
    try {
        let resp:team[] = (await axios.get(baseurl + "/teams")).data;
        AllTeams.update(value => {
            return resp
        })
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
}

export const editTeam = async (id, name, description, members:string[]) => {
    try {
        await axios.post(baseurl + "/teams/edit",{
            "id": id,
            "name": name,
            "description": description,
            "members": members});
        await getTeams();
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
}

export const authenticateUser = async ( username, password ) => {
    axios.defaults.headers.common['Authorization'] = `Basic ${btoa(username + ":" + password)}`;
    try {
        const resp = await axios.get(baseurl + "/userid");
        current_user.setId(resp.data);
        await getUserDetails();
        await getUserEvents();
        await getTeams();
        return true;
    } catch (err) {
        // Handle Error Here

        console.error(err);
        return false;
    }
}