import {writable} from "svelte/store";

export let today = writable(new Date());

export let daysSt = writable([])

export let monthForTool = writable(new Date().getMonth())

export let yearForTool = writable(new Date().getFullYear())

export const modal = writable(null);

export const windowStyle = writable({});

export enum main_page_state {
    start,
    calender,
    teams,
    organisations,
    settings,
    sign_up,
    log_in,
    change_email,
    privacyPolicy,
    impressum,
    rest
}

export class Page_State{
    page: main_page_state;
    constructor() {
        this.page = main_page_state.start;
    }

    setPage(newPage:main_page_state){
        this.page = newPage;
    }
}

export let current_page_state = writable(new Page_State());

export class AuthObj{
    auth: boolean;
    constructor() {
        this.auth = false;
    }

    setAuth(){
        this.auth = true;
    }
}

export let authenticated = writable(new AuthObj())

let authLocal: AuthObj;
authenticated.subscribe(value => {
    authLocal = value;
})

export class UserData{
    username: string;
    email: string;
    id: string;

    constructor() {
        this.username = null;
        this.email = null;
        this.id = null;
    }

    setId(id){
        authenticated.update((value) => {
            value.setAuth();
            console.log(value.auth)
            return value;
        })
        this.id = id;
    }

    setEmail(email){
        this.email = email;
    }

    setUsername(username){
        this.username = username;
    }
}

export let user = writable(new UserData());

export interface EventObj{
    eventId: string
    eventType: string
    title: string
    notes: string
    startDate: string
    endDate: string
    organizer: string
    team: string
    recurrence: string
}

export let UserEvents = writable([]);

export interface CreateEventObj{
    eventType: string;
    title: string;
    notes: string;
    startDate: string;
    endDate: string;
    team: string;
    recurrence: string;
}

export interface user{

}

export interface team{
    id: string
    name: string
    description: string
    members: []
}

export let AllTeams = writable([])
